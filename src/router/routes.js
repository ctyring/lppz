const routes = [{
        path: '/',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: 'login',
                component: () =>
                    import ('pages/login.vue')
            },
            {
                path: 'register',
                component: () =>
                    import ('pages/register.vue')
            },
            {
                path: 'works',
                component: () =>
                    import ('pages/works.vue')
            },
            {
                path: 'person',
                component: () =>
                    import ('pages/person.vue')
            },
            {
                path: '',
                component: () =>
                    import ('pages/Index.vue')
            }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () =>
            import ('pages/Error404.vue')
    }
]

export default routes