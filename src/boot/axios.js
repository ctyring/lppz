import { boot } from 'quasar/wrappers'
import axios from 'axios'
import store from "src/store";
import router from 'src/router/index';
// import Notify from 'quasar';

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)

const api = axios.create({ baseURL: 'http://47.117.163.229:9999' })
api.interceptors.request.use(config => {
        config.headers = {
            'X-Access-Token': store().getters.GET_TOKEN
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    })

api.interceptors.response.use(function(response) {
    // 对响应数据做点什么
    return response;
}, function(error) {
    // 对响应错误做点什么
    if (error.response.status === 401) {
        // Notify.create({
        //     message: "token失效，请重新登录",
        //     color: 'red',
        //     position: 'top'
        // })
        router().push('/login')

        window.localStorage.clear();
        // return Proimse.reject()
    }
    return Promise.reject(error);
})
export default boot(({ app }) => {
    // for use inside Vue files (Options API) through this.$axios and this.$api

    app.config.globalProperties.$axios = axios
        // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
        //       so you won't necessarily have to import axios in each vue file

    app.config.globalProperties.$api = api
        // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
        //       so you can easily perform requests against your app's API
})

export { api }